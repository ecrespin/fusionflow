/**
  Copyright (C) 2012-2015 by Autodesk, Inc.
  All rights reserved.

  OMAX waterjet post processor configuration.

  $Revision: 40906 59ea17daaed3dd32c7f278e467c4c9d7a54865b7 $
  $Date: 2015-12-16 07:58:47 $
  
  FORKID {A806DFCD-07A4-4506-9AF6-04397CF5BF05}
*/

/*
  ATTENTION:

  1. The initial head position must always match the WCS XY origin
  specified in the CAM Setup.

  2. The initial Z head position must always be zeroed just above the top of the stock.
*/

/* Flow Format that this Post Generates:

Aug 3 2016
TODO: 1. jet on/off: take into account jet status. I think it can be controlled by the calls to onMovement 
TODO: 2. speed: control the speed. Is it by setting a quality and defining quality values at design in Fusion ?
TODO: 3. tabs: check how tabs are related to post function calls or geometry
TODO: 4. lead in/out: analize native ORD files for lead/in lead/out specification

Aug 5 2016
TODO: 1. speed: control the speed. Is it by setting a quality and defining quality values at design in Fusion ?
TODO: 2. tabs: shift out the gap makeing the tab. In the direction perpendicular to the segment defined by 
         the start and finish points of the path and make cuts joining the original path and the shifted gap.

Aug 7 2016
TODO: 1. tabs: shift out the gap makeing the tab. In the direction perpendicular to the segment defined by 
         the start and finish points of the path and make cuts joining the original path and the shifted gap.
         ask if there is experience with other shape tabs directly in fusion 360 first before doing it.
TODO: 2. right/none/left kerf compensation: shift the nozzle to compensate for the kerf. (shape shrink due to diameter of the jet).

The kerf compensation is found to work with the field before the last that I had called compensation. In fact it is 1 for right compensation
and -1 for left compensation and 0 for no compensation. So far the path works fine when counterclockwise. If change to clockwise by swaping the
red arrow direction, the lead in/lead out path is done in the opsite side. Maybe I lack some experience.... but at least with left compensation 
and counterclockwise we are good.

Aug 9 2016
TODO: 1. tabs: shift out the gap makeing the tab. In the direction perpendicular to the segment defined by 
         the start and finish points of the path and make cuts joining the original path and the shifted gap.
         ask if there is experience with other shape tabs directly in fusion 360 first before doing it.

In ORD files a line is a sequence of real (float) values. Depending on the position of the value in the line its value has a meaning:

     Pos   Code Name             Meaning
    ====  =========== =================================================================
     0        x         x coord start of segment or begin/end of arc depending of shape (pos 3 value)
     1        y         y coord start of segment or begin/end of arc depending of shape (pos 3 value)
     2        z         z height
     3        shape      straight line or arc
                              0: path is a straignt segment. 
                              1: path is an clockwise arc from (ix iy) to next(ix iy) centered at cx, cy. 
                             -1: path is an counter clockwise arc from (ix iy) to next(ix iy) centered at cx, cy.
     4        speed        speed. In samples seen, speed is 0, 40 or 5900. 40 for regular speed. Its the % of speed respect to the material standard
                           cutting speed.
     5        compensation  -1: left,  1: right. 0 : none. With respect to the direction of the cut vector
     6        jetaction        jet action. Research a bit more. 
                             -1 = lead in, jet on. 
                              2 = cut, jet on. 
                              1 = lead out, jet on.  
                              0 = jet off
                             -2 = cut, jet on 
     7        cx        center of arc x coord. only exists if shape is arc
     8        cy        center of arc x coord. only exists if shape is arc
     9        cz        center of arc x coord. only exists if shape is arc

*/

// Fusion 360 cam post documentation found at https://github.com/AutodeskCAM/Documentation

description = "FLOW Waterjet experimental";
vendor = "FLOW";
vendorUrl = "https://www.flowwaterjet.com";
legal = "Copyright (C) 2012-2015 by Autodesk, Inc.";
certificationLevel = 0;
minimumRevision = 10000;
extension = "ord";
setCodePage("ascii");

longDescription = "Post for FLOW Waterjet. 1) The initial head position must always match the WCS XY origin specified in the CAM Setup. 2) The initial Z head position must always be zeroed just above the top of the stock.";

// unit = IN; // only inch mode is supported
capabilities = CAPABILITY_MILLING | CAPABILITY_JET; // milling will be removed
tolerance = spatial(0.002, MM);

minimumChordLength = spatial(0.01, MM);
minimumCircularRadius = spatial(0.01, MM);
maximumCircularRadius = spatial(1000, MM);
minimumCircularSweep = toRad(0.01);
maximumCircularSweep = toRad(180);
allowHelicalMoves = false;
allowedCircularPlanes = 1 << PLANE_XY; // allow only XY circular motion

properties = {
  etchQuality: 6, // Etch quality
  tabLength: 1.5, // Length of tabs
  debug: false, // debug flag
  reverse: false // reverse path sense in each closed contour
};


// use fixed width instead
var xyzFormat = createFormat({decimals:4, trim:false});
var bowFormat = createFormat({decimals:4, trim:false});
var tFormat = createFormat({decimals:4, trim:false});
var integerFormat = createFormat({decimals:0});

// fixed settings
var preamble = true;

var qualFeed = 0; // turn off waterjet
var tas = 0; // unused tilt start
var tae = 0; // unused tilt end
var thickness = 0;

// used for delaying moves
var xTemp = 0;
var yTemp = 0;
var zTemp = 0;
var bowTemp = 0;
var gotMove = false;
var cxTemp = 0;
var cyTemp = 0;
var czTemp = 0;
var arcTemp = false;
var pathtypeTemp = 0;
var feedTemp = 0;
var compensationTemp = 0;
var jetactionTemp = 0;

// constants
// CONSTANTS
// =========
//line position of different fields
var POS_X = 0;
var POS_Y = 1;
var POS_Z = 2;
var POS_SHAPE = 3;
var POS_SPEED = 4;
var POS_COMPENSATION = 5;
var POS_JETACTION = 6;
var POS_CX = 7;
var POS_CY = 8;
var POS_CZ = 9;

//possible action parameter values. these match the ORD file

//shape
var STRAIGHT =  0;
var ARC1 =      1;
var ARC2 =     -1;

//clockwise
var CLOCKWISE        = -1;
var COUNTERCLOCKWISE =  1;

//jetaction
var JET_OFF    =  0;
var LEAD_IN    = -1;
var CUT_PATH_1 = -2;
var CUT_PATH_2 =  2;
var LEAD_OUT   =  1;

var numArcs = 0;
var numLines = 0

// override radius compensation
var compensationOffset = 0; // center compensation
var feedRate = 60;
var retractHeight = 5;

var etchOperation = false; // though-cut unless set to true

var EOB = "";

function Action (x, y, z, shape, speed, compensation, jetaction, arcFlag,  cx, cy, cz) {
        this.x         =  x;
        this.y         =  y;
        this.z         =  z;
        this.shape     =  shape;
        this.speed     =  speed;
        this.compensation =  compensation;
        this.jetaction =  jetaction;
        this.arcFlag   =  typeof arcFlag != 'undefined' ? arcFlag : false;
        this.cx        =  typeof cx != 'undefined' ? cx : 0;
        this.cy        =  typeof cy != 'undefined' ? cy : 0;
        this.cz        =  typeof cz != 'undefined' ? cz : 0;
  }
  
Action.prototype.get_action = function(){
   return {
       x : this.x,
       y : this.y,
       z : this.z,
       shape : this.shape,
       speed : this.speed,
       compensation : this.compensation,
       jetaction : this.jetaction,
       arcFlag : this.arcFlag,
       cx : this.cx,
       cy : this.cy,
       cz : this.cz
    };
};

var seq = [];

function WriteLogMessage(msg) {
    if(properties.debug)
        writeln("//"+msg);
}

// writes the specified block.
function writeBlock() {
//  writeWords2("[0], ", arguments, EOB);
  writeWords2("", arguments, EOB);
}

function JSDateToExcelDate(date) {
  var returnDateTime = 25569.0 + ((date.getTime() - (date.getTimezoneOffset() * 60 * 1000)) / (1000 * 60 * 60 * 24));
  return returnDateTime.toString().substr(0, 5);
}

var FIELD = "                    ";

/** Make sure fields are aligned. */
function f(text) {
  var length = text.length;
  if (length > 10) {
    return text;
  }
  return FIELD.substr(0, 10 - length) + text;
}

/** Make sure fields are aligned. */
function fi(text, size) {
  var length = text.length;
  if (length > size) {
    return text;
  }
  return FIELD.substr(0, size - length) + text;
}

function onOpen() {
  
  if ((properties.etchQuality != 6) && (properties.etchQuality != 7)) {
    error(localize("Etch quality must be either 6 or 7."));
    return;
  }

  setWordSeparator("");
  WriteLogMessage("(code)IN="+IN+", (code)MM="+MM+", unit="+unit);
  switch (unit) {
  case IN:
    // Do nothing Omax files can only be in IN
    break;
  case MM:
    xyzFormat = createFormat({decimals:4, trim:false, scale:1/25.4}); // convert to inches 
    // error(localize("Program must be in inches."));
    // return;
    break;
  }

  { // stock - workpiece
    var workpiece = getWorkpiece();
    var delta = Vector.diff(workpiece.upper, workpiece.lower);
    if (delta.isNonZero()) {
      // thickness = (workpiece.upper.z - workpiece.lower.z);
    }
  }
  
  if (preamble) {
    WriteLogMessage("// Slash Slash marks (//) represent remarks that are ignored by the computer");
    WriteLogMessage("// if the slashes are the first characters in the line");
    WriteLogMessage("// Below is the version of this file format. Do not modify it, unless you are confident you know what you are doing.");
    WriteLogMessage("//" + f("X") + "  " + f("Y") + "  " + f("Z") + "  " + fi("Shape",6) + "  " + fi("Speed",6) + "  " + fi("CW/CCW",6) + "  " + fi("Jet", 8) + "  " + f("centerX") + "  " + f("centerY") + "  " + fi("centerZ"));
  }

  if (getNumberOfSections() > 0) {
    var firstSection = getSection(0);

    var remaining = firstSection.workPlane;
    if (!isSameDirection(remaining.forward, new Vector(0, 0, 1))) {
      error(localize("Tool orientation is not supported."));
      return;
    }
    setRotation(remaining);

    var originZ = firstSection.getGlobalZRange().getMinimum(); // the cutting depth of the first section

    for (var i = 0; i < getNumberOfSections(); ++i) {
      var section = getSection(i);
      var z = section.getGlobalZRange().getMinimum();
      if ((z + 1e-9) < originZ) {
        error(localize("You are trying to machine at multiple depths which is not allowed."));
        return;
      }
    }
  }


  if (getNumberOfSections() > 0) {
    var firstSection = getSection(0);
    var initialPosition = getFramePosition(firstSection.getInitialPosition());

      xTemp = initialPosition.x;
      yTemp = initialPosition.y;
      zTemp = initialPosition.z;
  }
  pathtypeTemp = STRAIGHT;
}

function onSection() {
  var remaining = currentSection.workPlane;
  if (!isSameDirection(remaining.forward, new Vector(0, 0, 1))) {
    error(localize("Tool orientation is not supported."));
    return;
  }
  setRotation(remaining);

if (hasParameter("operation:tool_feedCutting"))
{
  feedRate = getParameter("operation:tool_feedCutting");
}

if (hasParameter("operation:retractHeight_value"))
{
  retractHeight = getParameter("operation:retractHeight_value");
}


  etchOperation = false;
  if (currentSection.getType() == TYPE_JET) {

    switch (tool.type) {
    case TOOL_WATER_JET:
      break;
    default:
      error(localize("The CNC does not support the required tool."));
      return;
    }

    switch (currentSection.jetMode) {
    case JET_MODE_THROUGH:
      break;
    case JET_MODE_ETCHING:
      etchOperation = true;
      break;
    case JET_MODE_VAPORIZE:
      error(localize("Vaporize is not supported by the CNC."));
      return;
      break;
    default:
      error(localize("Unsupported cutting mode."));
      return;
    }

    // currentSection.quality

  } else if (currentSection.getType() == TYPE_MILLING) {
    warning(localize("Milling toolpath will be used as waterjet through-cutting toolpath."));
  } else {
    error(localize("CNC doesn't support the toolpath."));
    return;
  }
}

function onParameter(name, value) {
}

var currentQual = 0;
var currentCompensationOffset = 0; // center compensation

function memorizeMove() {
        WriteLogMessage("memorizeMove():"+xTemp+","+yTemp+","+zTemp);
        if(arcTemp) {
          action = new Action(xTemp, yTemp, zTemp, pathtypeTemp, feedTemp, compensationTemp, jetactionTemp,arcTemp, cxTemp,cyTemp,czTemp);
          numArcs++;
        }
        else {
          action = new Action(xTemp, yTemp, zTemp, pathtypeTemp, feedTemp, compensationTemp, jetactionTemp);
          numLines++;
        }
        seq.push(action);
}

function getMemorizedMove(i) {
      return(seq[i]);
}

function setMemorizedMove(i,action) {
      seq[i] = action;
}


function flushMove(x,y,z,action) {
  if (gotMove) {
    if ((currentQual == 0) || (qualFeed == 0)) {
      // the alternative would be to skip the moves where the compensation is changed - but this would cause the lead in/out to be different
      currentCompensationOffset = compensationOffset; // compensation offset may only be changed during quality 0
    }
    currentQual = qualFeed;
//    if (currentCompensationOffset != compensationOffset) {
//      writeComment("Compensation mismatch");
//    }

    validate(xyzFormat.getResultingValue(action.z) >= 0, "Toolpath is unexpectedly below Z0."); // just for safety
    writeBlock(
      f(xyzFormat.format(x)), ",",
      f(xyzFormat.format(y)), ",",
      f(xyzFormat.format(z)), ",",
      fi(integerFormat.format(action.shape), 2), ",",
      fi(integerFormat.format(action.speed), 5), ",",
      fi(integerFormat.format(action.compensation), 2), ",",
      fi(integerFormat.format(action.jetaction), 2), 
      conditional(action.arcFlag, ","),
      conditional(action.arcFlag, f(xyzFormat.format(action.cx))),
      conditional(action.arcFlag, ","),
      conditional(action.arcFlag, f(xyzFormat.format(action.cy))),
      conditional(action.arcFlag, ","),
      conditional(action.arcFlag, f(xyzFormat.format(action.cz)))
    );
    gotMove = false;
    bowTemp = 0;
    pathtypeTemp = 0;
  }
}

function onRapid(x, y, z) {
  WriteLogMessage("onRapid");
  feedTemp = 0;
    qualFeed = 0; // turn off waterjet
    jetactionTemp = JET_OFF;
//  compensationTemp = 0;
//  jetactionTemp = 0;
  bowTemp = 0;
  arcTemp = false;
/*
  gotMove = true;
  memorizeMove();
*/
  xTemp = x;
  yTemp = y;
  zTemp = z;
  zTemp = retractHeight;
  feedTemp = 0;
    qualFeed = 0; // turn off waterjet
      jetactionTemp = JET_OFF;

  
}

function adjustTab(x,y,z,feed) {
        // writeln("tab length: "+properties.tabLength);
        tmp_pathtypeTemp = pathtypeTemp;
        pathtypeTemp = STRAIGHT;
        arcTemp = false;
        feedTemp = feedRate;
        compensationTemp = 0;
        gotMove = true;
            // tab_side1 p1 = xTemp,yTemp,zTemp (current values)
        zTemp = retractHeight;
        feedTemp = 0;
    qualFeed = 0; // turn off waterjet
    jetactionTemp = JET_OFF;
        memorizeMove();
        normal_z = new Vector(0,0,-1); // could be any vector in z direction. The cross product between this vector and 
                                       // any vector t on the XY plane, is a new vector p on the XY plane, perpendicular to t.
        // our t is the tab side vector 'tab_vector' that will be calculated as the vector difference 
        // of the path gap begin and end points that are separated by the tab. Namely  points xTemp,yTemp,zTemp and x,y,z
        // For this we first create two vectors: vectorP1 and vectorP2, from the coordinates of the two last mentioned points.
            vectorP1 = new Vector(xTemp,yTemp,zTemp);
        //    writeln("P1: ("+xTemp+","+yTemp+","+zTemp+")");
            vectorP2 = new Vector(x,y,z);
    //    writeln("P2: ("+x+","+y+","+z+")");
        // The difference of these two vectors is the vector of the gap the tab creates in the path
            tab_vector = Vector.diff(vectorP2,vectorP1);

        // now we calculate the vector that represents the segment from the tab points on the path and the tabs end
        // for this we calculate p as stated above and normalize it. Then scale it by the factor of the tab's length
            // p = normalize the perpendicular to tab_vector on XY plane (cross product with tab_vector and 0,0,-1) 
            p = Vector.cross(tab_vector,normal_z).normalized;
            // shift_v = scale p to parameter.tabLength
            // scale it  
            p = Vector.product(p,properties.tabLength);

        // tab_side1 p2 = add shift_v to xTemp,yTemp,zTemp


        pathtypeTemp = tmp_pathtypeTemp;

        feedTemp = 5900;
        feedTemp = 0;
        jetactionTemp = LEAD_IN; // go down
        zTemp = 0;
        
        // NewLine p1 = add shift_v to x,y,z

        compensationTemp = 0;
        gotMove = true;
        memorizeMove();  
        zTemp = 0;
        jetactionTemp = CUT_PATH_1;
        feedTemp = feedRate;

        memorizeMove();


        // NewLine p1 = tab_side1 p2
        // NewLine p2 = add shift_v to x,y,z

        pathtypeTemp = STRAIGHT;
        arcTemp = false;
    
        feedTemp = 0;
        compensationTemp = 0;
        
        // x2,y2,z2 = add shift_v to x,y,z
        xTemp = x ;
        yTemp = y ;
        zTemp = z ;
        zTemp = 0;
        jetactionTemp = LEAD_OUT;
}

function onLinear(x, y, z, feed) {
  WriteLogMessage("onLinear");
  pathtypeTemp = 0;
//  feedTemp = feed;
//  compensationTemp = 0;
//  jetactionTemp = 0;
    bowTemp = 0;
    arcTemp = false;

    savedCompensationTemp = compensationTemp;

    if(feedTemp == 5900) // Tab
    {
        adjustTab(x,y,z,feed);
        gotMove = true;
        memorizeMove();
    }
    compensationTemp = savedCompensationTemp;
    xTemp = x;
    yTemp = y;
    zTemp = z;
    zTemp = retractHeight;
    feedTemp = 0;
    qualFeed = 0; // turn off waterjet
    jetactionTemp = JET_OFF;
//    action = new Action(xTemp, yTemp, zTemp, pathtypeTemp, feedTemp, compensationTemp, jetactionTemp);

}

function onCircular(clockwise, cx, cy, cz, x, y, z, feed) {
  // spirals are not allowed - arcs must be < 360deg
  // fail if radius compensation is changed for circular move
  WriteLogMessage("onCircular("+clockwise+","+cx+","+cy+","+cz+","+x+","+y+","+z+","+feed+")");
//  validate(gotMove, "Move expected before circular move.");
//  validate(bowTemp == 0);
//  bowTemp = (clockwise ? -1 : 1) * Math.tan(getCircularSweep()/4); // ATTENTION: setting bow for the pending move!
/*
  arcTemp = true;
  pathtypeTemp = (clockwise ? 1 : -1);
//  feedTemp = feed;
//  clockwiseTemp = (clockwise ? 1 : -1);
//  jetactionTemp = 0;


  cxTemp = cx;
  cyTemp = cy;
  czTemp = cz;
  czTemp = retractHeight;
  feedTemp = 0;
    qualFeed = 0; // turn off waterjet
    jetactionTemp = JET_OFF;

  bowTemp = 0;

  //  if(feedTemp == 5900) // Tab
  //      adjustTab(x,y,z,feed);

  gotMove = true;
  zTemp = retractHeight;
  feedTemp = 0;
    qualFeed = 0; // turn off waterjet
    jetactionTemp = JET_OFF;
  memorizeMove();
*/
  xTemp = x;
  yTemp = y;
  zTemp = z;
  zTemp = retractHeight;
  feedTemp = 0;
    qualFeed = 0; // turn off waterjet
    jetactionTemp = JET_OFF;



}

function onCommand(command) {
    WriteLogMessage("onCommand COMMAND="+command);
    switch (command) {
        case COMMAND_POWER_ON: // jet on 
            break;
        case COMMAND_POWER_OFF: // jet off
            break;
    }
}

function onMovement(movement) {
  switch (movement) {
  case MOVEMENT_CUTTING:
    jetactionTemp = CUT_PATH_1;  // check when it should be CUT_PATH_2
    WriteLogMessage("//onMovement MOVEMENT_CUTTING");
    feedTemp = feedRate;
    feedTemp = 0;
    qualFeed = 0; // turn off waterjet
    jetactionTemp = JET_OFF;
    break;
  case MOVEMENT_LINK_DIRECT:
    WriteLogMessage("//onMovement MOVEMENT_LINK_DIRECT");
    feedTemp = 5900;
    break;
  case MOVEMENT_FINISH_CUTTING:
    WriteLogMessage("//onMovement MOVEMENT_FINISH_CUTTING");
    jetactionTemp = CUT_PATH_1;
    if (etchOperation) {
      qualFeed = properties.etchQuality;
    } else {
      switch (currentSection.quality) {
      case 1:
        qualFeed = 5;
        break;
      case 2:
        qualFeed = 4;
        break;
      case 3:
        qualFeed = 2;
        break;
      default:
        qualFeed = 3;
      }
      break;  
    }
    break;
  case MOVEMENT_LEAD_IN:
    WriteLogMessage("onMovement LEAD IN");
    jetactionTemp = LEAD_IN;
    feedTemp = 0;
    qualFeed = 0; // turn off waterjet
    jetactionTemp = JET_OFF;
    break;
  case MOVEMENT_LEAD_OUT:
    WriteLogMessage("onMovement LEAD OUT");
    jetactionTemp = LEAD_OUT;
    feedTemp = 0;
    qualFeed = 0; // turn off waterjet
    jetactionTemp = JET_OFF;
    if (etchOperation) {
      qualFeed = properties.etchQuality;
    } 
    else {
      qualFeed = 9; // make a user defined setting
    }
    break;
  case MOVEMENT_RAPID:
    WriteLogMessage("onMovement MOVEMENT RAPID");
    jetactionTemp = JET_OFF;
    feedTemp = 0;
    qualFeed = 0; // turn off waterjet
    break;
  default:
    WriteLogMessage("onMovement ****Unknown****");
    jetactionTemp = JET_OFF;
    if (etchOperation) {
      qualFeed = properties.etchQuality;
    } else {
      qualFeed = 0; // turn off waterjet - used for head down moves
    }
  }
}

function onRadiusCompensation() {
    WriteLogMessage("onRadiousCompensation:"+radiusCompensation);
  switch (radiusCompensation) {
  case 1: // SIDEWAYS_COMPENSATION_LEFT
    compensationOffset = 1;
    compensationTemp = -1;
    break;
  case 2: // SIDEWAYS_COMPENSATION_RIGHT
    compensationOffset = 2;
    compensationTemp = 1;
    break;
  default:
    compensationOffset = 0; // center compensation
    compensationTemp = 0;
  }
}

function onCycle() {
    WriteLogMessage("onCycle");
  error(localize("Canned cycles are not supported."));
}

function onSectionEnd() {
    WriteLogMessage("onSectionEnd");
  qualFeed = 0; // turn off waterjet
  compensationOffset = 0; // center compensation
/*
  if (!gotMove) {
    var p = getCurrentPosition();
    xTemp = p.x;
    yTemp = p.y;
    zTemp = p.z;
    bowTemp = 0;
    arcTemp = false;
    gotMove = true;
  }
*/
//  validate(gotMove, "Move expected at end of operation to turn off waterjet.");
//  memorizeMove();
}

function flushAllMoves() {
    var arrayLength = seq.length;
    for (var i = 0; i < arrayLength; i++) {
        curAction = getMemorizedMove(i);
        qualFeed = 5; // must change qualFeed management to adjust to varying speeds 
        currentQual = 5;
        gotMove = true;
        flushMove(curAction.x,curAction.y,curAction.z,curAction);
    }
}

function flushAllMovesReverse() {
    var arrayLength = seq.length;
    for (var i = arrayLength-1; i > 0; i--) {
        curAction = getMemorizedMove(i-1);
        prevAction = getMemorizedMove(i);
        curAction.pathtypeTemp *= -1; 
        curAction.shape *= -1;
        curAction.compensation *= -1;
/*
        if(curAction.jetaction == CUT_PATH_1) { // inverse lead in/lead out movements
            curAction.jetaction = CUT_PATH_2;
        }
        else 
*/
        if(curAction.jetaction == LEAD_IN) { // inverse lead in/lead out movements
            curAction.jetaction = LEAD_OUT;
        }
        else if(curAction.jetaction == LEAD_OUT) {
            curAction.jetaction = LEAD_IN;
        }
        
        qualFeed = 5; // must change qualFeed management to adjust to varying speeds 
        currentQual = 5;
        gotMove = true;
        flushMove(prevAction.x,prevAction.y,prevAction.z,curAction);
    }
}

// Determine the last move in the next lead out starting at i_lead_in
function determineLastNextLeadOut(i_lead_in) {
    not_found = true; 
    lead_out_found = false;
    i_lead_out = -1;
    i = i_lead_in+1;
    while(not_found && (i < seq.length))
    {
        locAction = getMemorizedMove(i);
        if(locAction.jetaction == LEAD_OUT) // if lead out, flag it
            lead_out_found = true;
        else { // not lead out. if lead out was found then previous move was last lead out
            if(lead_out_found) {
                not_found = false;
                i_lead_out = i-1;
            }
        } 
        i++;
    }
    return i_lead_out;
}

function flushAllMovesReverse_old() {
    var arrayLength = seq.length;
    qualFeed = 5; // must change qualFeed management to adjust to varying speeds 
    currentQual = 5;
    for (var i = 0; i < arrayLength; i++) {
        curAction = getMemorizedMove(i);
        if(curAction.jetaction == LEAD_IN) // reverse path
        {
            i_lead_in = i;
            i_lead_out = determineLastNextLeadOut(i_lead_in);
            if(i_lead_out < 0) // if lead_out not found, reverse from end of movements
                i_lead_out = arrayLength - 1;
            WriteLogMessage("Reverse: "+i_lead_out+" downto "+i_lead_in);
            for(var j=i_lead_out; j >= i_lead_in; j--)
            {
                curRevAction = getMemorizedMove(j);
                curRevAction.pathtypeTemp *= -1; 
                curRevAction.compensation *= -1; 
                if(curRevAction.jetaction == LEAD_IN) { // inverse lead in/lead out movements
                    curRevAction.jetaction = LEAD_OUT;
                }
                else if(curRevAction.jetaction == LEAD_OUT) {
                    curRevAction.jetaction = LEAD_IN;
                }
                gotMove = true;
                flushMove(curRevAction);
            }
            i = i_lead_out;
        }
        else
        {
    /*
            xTemp = curAction.x;
            yTemp = curAction.y;
            zTemp = curAction.z;
            pathtypeTemp = curAction.shape; 
            feedTemp = curAction.speed; 
            compensationTemp = curAction.compensation;
            jetactionTemp = curAction.jetaction;
            arcTemp = curAction.arcFlag;
            cxTemp = curAction.cx;
            cyTemp = curAction.cy;
            czTemp = curAction.cz;
            qualFeed = 5; // must change qualFeed management to adjust to varying speeds 
            currentQual = 5;
    //        writeln("going to flushMove with "+xTemp+","+yTemp+","+zTemp);
    */
            gotMove = true;
            flushMove(curAction);
        }
    }
}

function onClose() {
    WriteLogMessage("onClose");
    numLines--;
    writeln("// This file was created by FlowMaster(R), which is proprietary to Flow International Corporation. "+numLines+" "+numArcs);
    writeln("VER 6.00");
    if( properties.reverse )
        flushAllMovesReverse();
    else
        flushAllMoves();
}
